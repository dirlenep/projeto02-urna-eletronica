var brancos = 0;
var nulos = 0;
var candidato1 = 0;
var candidato2 = 0;
var candidato3 = 0;
var candidato4 = 0;
var candidato5 = 0;
var candidato6 = 0;
var candidato7 = 0;
var candidato8 = 0;
var candidato9 = 0;

// Confirmação de voto 
function votoNormal(){
    let candidato = document.querySelector("#idCandidatos").value;

    if (candidato == 11111){
        candidato1++
    } else if (candidato == 22222) {
        candidato2++
    } else if (candidato == 33333) {
        candidato3++
    } else if (candidato == 44444) {
        candidato4++
    } else if (candidato == 55555) {
        candidato5++
    } else if (candidato == 66666) {
        candidato6++
    } else if (candidato == 77777) {
        candidato7++
    } else if (candidato == 88888) {
        candidato8++
    } else if (candidato == 99999) {
        candidato9++
    } else if (candidato == "") {
        brancos++
    } else {
        nulos++
    }
    resetar();
}

function mostrarCandidato(){
    let nomeCandidato 
    let candidato = document.querySelector("#idCandidatos").value;

    if (candidato == 11111){
        nomeCandidato = "Candidato1"
    } else if (candidato == 22222) {
        nomeCandidato = "Candidato2"
    } else if (candidato == 33333) {
        nomeCandidato = "Candidato3"
    } else if (candidato == 44444) {
        nomeCandidato = "Candidato4"
    } else if (candidato == 55555) {
        nomeCandidato = "Candidato5"
    } else if (candidato == 66666) {
        nomeCandidato = "Candidato6"
    } else if (candidato == 77777) {
        nomeCandidato = "Candidato7"
    } else if (candidato == 88888) {
        nomeCandidato = "Candidato8"
    } else if (candidato == 99999) {
        nomeCandidato = "Candidato9"
    } else if (candidato == "") {
        nomeCandidato = "Voto em Branco"
    } else {
        nomeCandidato = "Voto Nulo"
    }
    document.getElementById("imagemCandidato").src = "img/" + nomeCandidato + ".jpg"
    document.getElementById("idNomeCandidato").value = nomeCandidato
}

// Voto em branco
function votoBranco(){
    brancos++;
    resetar();
}

function resetar(){
    document.getElementById("imagemCandidato").src = "img/candidato.jpg"
    document.getElementById("idNomeCandidato").value = ""
}